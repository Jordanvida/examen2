// App.js
import React, { useState } from 'react';
import { View, Button, StyleSheet } from 'react-native';
import SignIn from './SignIn';
import SignUp from './SignUp';

export default function App() {
  const [isSignIn, setIsSignIn] = useState(true);

  return (
    <View style={styles.container}>
      {isSignIn ? <SignIn /> : <SignUp />}
      <Button
        title={isSignIn ? 'Switch to Sign Up' : 'Switch to Sign In'}
        onPress={() => setIsSignIn(!isSignIn)}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
    backgroundColor: '#f0f0f0',
  },
});
