import React, { useState } from 'react';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { auth } from './firebaseConfig';
import SignInForm from './SignInForm';
//Inicio de sesion
const SignIn = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');

    const handleSignIn = () => {
        signInWithEmailAndPassword(auth, email, password)
            .then(() => {
                // Navegar a la pantalla de inicio
            })
            .catch((err) => {
                setError(err.message);
            });
    };

    return (
        <>
            <SignInForm
                email={email}
                setEmail={setEmail}
                password={password}
                setPassword={setPassword}
                handleSignIn={handleSignIn}
            />
            {error ? <Text style={styles.errorText}>{error}</Text> : null}
        </>
    );
};

const styles = StyleSheet.create({
    errorText: {
        color: 'red',
        marginTop: 16,
    },
});

export default SignIn;
