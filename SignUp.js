// SignUp.js
import React, { useState } from 'react';
import { createUserWithEmailAndPassword } from 'firebase/auth';
import { auth } from './firebaseConfig';
import SignUpForm from './SignUpForm';

const SignUp = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');

    const handleSignUp = () => {
        createUserWithEmailAndPassword(auth, email, password)
            .then(() => {
                // Navegar a la pantalla de inicio
            })
            .catch((err) => {
                setError(err.message);
            });
    };
    return (
        <>
            <SignUpForm
                email={email}
                setEmail={setEmail}
                password={password}
                setPassword={setPassword}
                handleSignUp={handleSignUp}
            />
            {error ? <Text style={styles.errorText}>{error}</Text> : null}
        </>
    );
};

const styles = StyleSheet.create({
    errorText: {
        color: 'red',
        marginTop: 16,
    },
});

export default SignUp;
